# Build binary
FROM --platform=$BUILDPLATFORM golang:1-alpine AS builder
LABEL org.opencontainers.image.authors="Duncan Hutty <dhutty@allgoodbits.org>"

ARG TARGETOS
ARG TARGETARCH

ENV GO111MODULE=on
ENV GOFLAGS=-mod=readonly

WORKDIR $GOPATH/src/gitlab.com/dhutty/echoprime
COPY . .

RUN export CGO_ENABLED=0 \
    && if [ "${TARGETOS}" = "darwin" ]; then \
         export CGO_ENABLED=1; \
       fi

RUN GOOS=${TARGETOS} \
    GOARCH=${TARGETARCH} \
      go build -ldflags="-s -w" -o /echoprime

# Create image
FROM scratch
LABEL maintainer="Duncan Hutty <dhutty@allgoodbits.org>"

WORKDIR /
COPY --link --from=builder /go/src/gitlab.com/dhutty/echoprime/ .
COPY --link --from=builder /echoprime .

EXPOSE 8080
ENTRYPOINT ["/echoprime"]
