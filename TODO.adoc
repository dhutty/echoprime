= TODO: echoprime
Duncan Hutty <dhutty@allgoodbits.org>
:modified: 2024-01-02 23:53
:source-highlighter: pygments

=== For the app itself

. [.line-through]#Simple MVP of a HTTP responder#
. Configure Gin via env, e.g: GIN_MODE=release
. add logging
. link:https://gowebexamples.com/http-server/[process the request params]
. consider link:https://gin-gonic.com/docs/examples/[Gin examples]

=== Not the app

. [.line-through]#Dockerfile#
. [.line-through]#docker compose#
. [.line-through]#GitLab CI pipeline#
. specify conventional commit
. automatically generate CHANGELOG
. .editorconfig
. Add linter to pre-commit and CI pipeline?
. write {unit,e2e} link:https://gin-gonic.com/docs/testing/[test]
. add test execution to CI pipeline
. dependabot or similar?
. Helm Chart
